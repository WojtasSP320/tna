#!/usr/bin/env python
# -*- coding: utf8 -*-

import time
import RPi.GPIO as GPIO
from beep import beep


class keyboard:

    POUT_ROW_1 = 29
    POUT_ROW_2 = 31
    POUT_ROW_3 = 33
    POUT_ROW_4 = 35

    PIN_COL_1 = 37
    PIN_COL_2 = 36
    PIN_COL_3 = 38
    PIN_COL_4 = 40

    sound = None

    # ~

    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.POUT_ROW_1, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.POUT_ROW_2, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.POUT_ROW_3, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.POUT_ROW_4, GPIO.OUT, initial=GPIO.LOW)
        # ~
        GPIO.setup(self.PIN_COL_1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.PIN_COL_2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.PIN_COL_3, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.PIN_COL_4, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        self.sound = beep()

    def __check_first_row(self):
        keys = ''
        GPIO.output(self.POUT_ROW_1, GPIO.HIGH)
        if GPIO.input(self.PIN_COL_1):
            keys = '1'
        elif GPIO.input(self.PIN_COL_2):
            keys = '2'
        elif GPIO.input(self.PIN_COL_3):
            keys = '3'
        elif GPIO.input(self.PIN_COL_4):
            keys = 'A'
        GPIO.output(self.POUT_ROW_1, GPIO.LOW)
        return keys

    def __check_second_row(self):
        keys = ''
        GPIO.output(self.POUT_ROW_2, GPIO.HIGH)
        if GPIO.input(self.PIN_COL_1):
            keys += '4'
        elif GPIO.input(self.PIN_COL_2):
            keys += '5'
        elif GPIO.input(self.PIN_COL_3):
            keys += '6'
        elif GPIO.input(self.PIN_COL_4):
            keys += 'B'
        GPIO.output(self.POUT_ROW_2, GPIO.LOW)
        return keys

    def __check_third_row(self):
        keys = ''
        GPIO.output(self.POUT_ROW_3, GPIO.HIGH)
        if GPIO.input(self.PIN_COL_1):
            keys += '7'
        elif GPIO.input(self.PIN_COL_2):
            keys += '8'
        elif GPIO.input(self.PIN_COL_3):
            keys += '9'
        elif GPIO.input(self.PIN_COL_4):
            keys += 'C'
        GPIO.output(self.POUT_ROW_3, GPIO.LOW)
        return keys

    def __check_fourth_row(self):
        keys = ''
        GPIO.output(self.POUT_ROW_4, GPIO.HIGH)
        if GPIO.input(self.PIN_COL_1):
            keys += '*'
        elif GPIO.input(self.PIN_COL_2):
            keys += '0'
        elif GPIO.input(self.PIN_COL_3):
            keys += '#'
        elif GPIO.input(self.PIN_COL_4):
            keys += 'D'
        GPIO.output(self.POUT_ROW_4, GPIO.LOW)
        return keys

    def keypad_check(self):
        keys = self.__check_first_row()
        if keys == '':
            keys = self.__check_second_row()
        if keys == '':
            keys = self.__check_third_row()
        if keys == '':
            keys = self.__check_fourth_row()

        if len(keys) > 0:
            self.sound.beep(0.05)
            while True:
                GPIO.output(self.POUT_ROW_1, GPIO.HIGH)
                GPIO.output(self.POUT_ROW_2, GPIO.HIGH)
                GPIO.output(self.POUT_ROW_3, GPIO.HIGH)
                GPIO.output(self.POUT_ROW_4, GPIO.HIGH)

                pressed = GPIO.input(self.PIN_COL_1) or GPIO.input(self.PIN_COL_2) or GPIO.input(self.PIN_COL_3) or GPIO.input(self.PIN_COL_4)
                if not pressed:
                    break
                else:
                    time.sleep(0.1)
        return keys

    def keypad_read(self):
        while True:
            key = self.__check_first_row()
            if key == '':
                key = self.__check_second_row()
            if key == '':
                key = self.__check_third_row()
            if key == '':
                key = self.__check_fourth_row()
            if len(key) > 0:
                self.sound.beep(0.05)
                break
            else:
                time.sleep(0.1)
        return key
