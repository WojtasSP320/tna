#!/usr/bin/env python
# -*- coding: utf8 -*-

import time
import RPi.GPIO as GPIO


class led:

    LED_RED = 11
    LED_GREEN = 13
    LED_BLUE = 7

    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.LED_RED, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.LED_GREEN, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.LED_BLUE, GPIO.OUT, initial=GPIO.LOW)

    def red_on(self):
        GPIO.output(self.LED_RED, GPIO.HIGH)

    def red_off(self):
        GPIO.output(self.LED_RED, GPIO.LOW)

    def green_on(self):
        GPIO.output(self.LED_GREEN, GPIO.HIGH)

    def green_off(self):
        GPIO.output(self.LED_GREEN, GPIO.LOW)

    def blue_on(self):
        GPIO.output(self.LED_BLUE, GPIO.HIGH)

    def blue_off(self):
        GPIO.output(self.LED_BLUE, GPIO.LOW)

    # RED
    def red(self, t):
        self.red_on()
        time.sleep(t)
        self.red_off()

    # GREEN
    def green(self, t):
        self.green_on()
        time.sleep(t)
        self.green_off()

    # BLUE
    def blue(self, t):
        self.blue_on()
        time.sleep(t)
        self.blue_off()

