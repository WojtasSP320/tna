#!/usr/bin/env python
# -*- coding: utf8 -*-

import signal
import time
import RPi.GPIO as GPIO

import datetime

import MFRC522
import RPi_I2C_driver

from keyboard import keyboard
from rest import rest
from led import led
from beep import beep


EVENT_WORK = 0
EVENT_BREAK = 1
EVENT_PRIVATE_OUTGO = 2

def end_read(signal, frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()


def xstr(s):
    return '' if s is None else str(s)

########################################################################################################################


continue_reading = True

backlight = 1

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

lcd = RPi_I2C_driver.lcd()
keyboard = keyboard()
led = led()
sound = beep()
rest = rest()
MIFAREReader = MFRC522.MFRC522()

led.blue(0.05)
sound.beep(0.05)
led.blue(0.05)
sound.beep(0.05)
time.sleep(0.5)
led.blue_on()

print "Maintenance..."

lcd.lcd_clear()
lcd.lcd_display_string("Przerwa,", 1)
lcd.lcd_display_string("techniczna...", 2)

# This loop keeps checking for chips. If one is near it will get the UID and authenticate
while continue_reading:

    time.sleep(1)
