#!/bin/bash

mkdir /usr/local/share/ca-certificates/danhoss.com
cd /usr/local/share/ca-certificates/danhoss.com
wget --no-check-certificate https://danline.system/danhoss.crt
update-ca-certificates
