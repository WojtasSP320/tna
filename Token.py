#!/usr/bin/env python
# -*- coding: utf8 -*-

import signal
import time
import RPi.GPIO as GPIO
import commands

import datetime

import MFRC522
import RPi_I2C_driver

from keyboard import keyboard
from rest import rest
from led import led
from beep import beep


EVENT_WORK = 0
EVENT_BREAK = 1
EVENT_PRIVATE_OUTGO = 2

def end_read(signal, frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()


def xstr(s):
    return '' if s is None else str(s)


def reset_cycle():
    display_date(':')


def display_date(time_separator):
    now = datetime.datetime.now()
    now_string = now.strftime('%d.%m.%Y %H' + time_separator + '%M   ')
    lcd.lcd_display_string(' Jestem czytnik ', 1)
    lcd.lcd_display_string(now_string, 2)
    lcd.backlight(1)


def handle_response(cid, response):
    if response['success']:
        lcd.lcd_clear()
        lcd.lcd_display_string(response['lcdMessage'][0], 1)
        lcd.lcd_display_string(response['lcdMessage'][1], 2)
        sound.beep_ok()
    else:
        lcd.lcd_clear()
        lcd.lcd_display_string('Ups... :(', 1)
        lcd.lcd_display_string('No i sie zepsulo', 1)
        sound.beep_err()

    time.sleep(1.5)

# ~


def log_work(cid, data):
    lcd.lcd_clear()
    lcd.lcd_display_string('Moment lacze sie', 1)
    lcd.lcd_display_string('z serwerem...', 2)
    response = rest.log_working_time(cid, 0)
    handle_response(cid, response)


def log_private_outgo(cid, data):
    lcd.lcd_clear()
    lcd.lcd_display_string('Moment lacze sie', 1)
    lcd.lcd_display_string('z serwerem...', 2)
    response = rest.log_working_time(cid, 2)
    handle_response(cid, response)

# ~


def start_work_event(cid, data):
    lcd.lcd_clear()
    lcd.lcd_display_string('Czesc ', 1)
    lcd.lcd_display_string(data['shortUsername'], 2)
    time.sleep(0.5)

    while True:
        lcd.lcd_clear()
        lcd.lcd_display_string('A: Do pracy', 1)
        key = keyboard.keypad_read()

        if key == 'A':
            log_work(cid, data)
        elif key == '#':
            lcd.lcd_clear()
            lcd.lcd_display_string('Token: ' + cid, 1)
            time.sleep(2)

        if key == 'A' or key == '*':
            reset_cycle()
            break


def choose_home_or_outgo(cid, data):
    lcd.lcd_clear()
    lcd.lcd_display_string('Czesc ', 1)
    lcd.lcd_display_string(data['shortUsername'], 2)
    time.sleep(0.5)

    while True:
        lcd.lcd_clear()
        lcd.lcd_display_string('A: Koniec roboty', 1)
        lcd.lcd_display_string('B: Wyjscie pryw.', 2)
        key = keyboard.keypad_read()

        if key == 'A':
            log_work(cid, data)
        elif key == 'B':
            log_private_outgo(cid, data)
        elif key == '#':
            lcd.lcd_clear()
            lcd.lcd_display_string('Token: ' + cid, 1)
            time.sleep(2)

        if key == 'A' or key == 'B' or key == '*':
            reset_cycle()
            break


def choose_work_or_home(cid, data):
    lcd.lcd_clear()
    lcd.lcd_display_string('Czesc ', 1)
    lcd.lcd_display_string(data['shortUsername'], 2)
    time.sleep(0.5)

    while True:
        lcd.lcd_clear()
        lcd.lcd_display_string('A: Wroc do pracy', 1)
        lcd.lcd_display_string('B: Koniec roboty.', 2)
        key = keyboard.keypad_read()

        if key == 'A':
            log_private_outgo(cid, data)
        elif key == 'B':
            log_work(cid, data)
        elif key == '#':
            lcd.lcd_clear()
            lcd.lcd_display_string('Token: ' + cid, 1)
            time.sleep(2)

        if key == 'A' or key == 'B' or key == '*':
            reset_cycle()
            break


def handle_card(cid):
    lcd.backlight(1)
    sound.beep(0.1)

    time.sleep(0.1)
    led.blue_off()

    lcd.lcd_clear()
    lcd.lcd_display_string('Sprawdzam', 1)
    lcd.lcd_display_string('kim jestes...', 2)

    data = rest.get_last_log(cid)
    if data['success']:
        print 'Log id: ' + str(data['log']['id'])
        print 'Start: ' + str(data['log']['start'])
        print 'End: ' + str(data['log']['end'])
        print 'In progress: ' + str(data['log']['inProgress'])
        print 'Type: ' + str(data['log']['type'])
        print 'Username: ' + data['username']

        print "\n---------------------\n"

        if (data['log']['present'] == False) or (data['log']['type'] == 0 and data['log']['inProgress'] == False):
            # No previous logs or previous log is work and finished
            start_work_event(cid, data)
        elif data['log']['type'] == 0 and data['log']['inProgress'] == True:
            # Previous log is type work and in progress set to true
            choose_home_or_outgo(cid, data)
        elif data['log']['type'] == 2 and data['log']['inProgress'] == True:
            # Previous log is type private outgo and in progress set to true
            choose_work_or_home(cid, data)
    elif data['success'] == False and data['userNotFound'] == True:
        lcd.lcd_clear()
        lcd.lcd_display_string('Sorry, ale', 1)
        lcd.lcd_display_string('my sie nie znamy', 2)
        sound.beep_err()
        time.sleep(0.5)
        key = keyboard.keypad_check()
        if key == '#':
            sound.beep(0.05)
            lcd.lcd_clear()
            lcd.lcd_display_string('Token: ' + cid, 1)
            time.sleep(1)
            key = ''
            while len(key) == 0:
                key = keyboard.keypad_check()
                time.sleep(0.1)
            sound.beep(0.05)
    elif data['message'] == 'bps':
        lcd.lcd_clear()
        lcd.lcd_display_string('Nie mam', 1)
        lcd.lcd_display_string('internetu :(', 2)
        sound.beep_err()
        time.sleep(1)
    elif data['message'] == '500':
        lcd.lcd_clear()
        lcd.lcd_display_string('Ups... :(', 1)
        lcd.lcd_display_string('Blad serwera', 2)
        sound.beep_err()
        time.sleep(1)
    else:
        lcd.lcd_clear()
        lcd.lcd_display_string('Ups... :(', 1)
        lcd.lcd_display_string('No i sie zepsulo', 2)
        sound.beep_err()
        time.sleep(1)
    reset_cycle()


def read_rfid():
    # Scan for cards
    (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # Get the UID of the card
    (status, uid) = MIFAREReader.MFRC522_Anticoll()

    # If we have the UID, continue
    if status == MIFAREReader.MI_OK:
        return uid
    else:
        return None


def show_ip():
    ip = commands.getoutput('hostname -I')
    lcd.lcd_clear()
    lcd.lcd_display_string('IP: ' + ip, 1)
    time.sleep(2)
    reset_cycle()


def show_ssid():
    ssid = commands.getoutput('iwgetid -r')
    lcd.lcd_clear()
    lcd.lcd_display_string('WiFi SSID name:', 1)
    lcd.lcd_display_string(ssid, 2)
    time.sleep(2)
    reset_cycle()


def show_signal():
    string = commands.getoutput('iwconfig wlan0 | grep -i Quality')

    x1 = string.find('=') + 1
    x2 = string.find(' ', x1 + 1)
    x3 = string.find('=', x2) + 1
    qty = string[x1:x2]
    lvl = string[x3:]
    lcd.lcd_clear()
    lcd.lcd_display_string('Quality: ' + qty, 1)
    lcd.lcd_display_string('Level: ' + lvl, 2)
    time.sleep(2)
    reset_cycle()


def show_place():
    if rest.parameters.place == 0:
        place ='Biuro'
    elif rest.parameters.place == 1:
        place ='Magazyn'
    elif rest.parameters.place == 2:
        place = 'Produkcja'
    else:
        place = 'Nie mam pojecia'

    lcd.lcd_clear()
    lcd.lcd_display_string('Miejsce:', 1)
    lcd.lcd_display_string(place, 2)
    time.sleep(2)
    reset_cycle()


def show_temperature():
    temp = commands.getoutput('/opt/vc/bin/vcgencmd measure_temp')

    lcd.lcd_clear()
    lcd.lcd_display_string(temp, 1)
    time.sleep(2)
    reset_cycle()


def shutdown():
    time.sleep(1)
    # key = keyboard.keypad_check()
    # if key == '9':
    lcd.lcd_clear()
    lcd.lcd_display_string('Zamykam system', 1)
    sound.beep(2)
    # key = keyboard.keypad_check()
    # if key == '9':
    string = commands.getoutput('shutdown -h now')
    lcd.lcd_display_string('shutdown -h now', 2)
    reset_cycle()


def handle_keyboard(key):
    global backlight

    if key == '0':
        if backlight == 1:
            backlight = 0
            lcd.backlight(0)
        else:
            backlight = 1
            lcd.backlight(1)

    elif key == '1':
        show_ip()
    elif key == '2':
        show_ssid()
    elif key == '3':
        show_signal()
    elif key == '4':
        show_place()
    elif key == '5':
        show_temperature()
    elif key == '9':
        shutdown()

########################################################################################################################


continue_reading = True

backlight = 1

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

lcd = RPi_I2C_driver.lcd()
keyboard = keyboard()
led = led()
sound = beep()
rest = rest()
MIFAREReader = MFRC522.MFRC522()

reset_cycle()

led.blue(0.05)
sound.beep(0.05)
led.blue(0.05)
sound.beep(0.05)
time.sleep(0.5)
led.blue_on()

print "Running..."

# This loop keeps checking for chips. If one is near it will get the UID and authenticate
while continue_reading:

    uid = read_rfid()
    if uid is not None:
        cid = "0x%0.2x%0.2x%0.2x%0.2x" % (uid[0], uid[1], uid[2], uid[3])
        now = datetime.datetime.now()
        print str(now)
        print "Token ID: " + cid
        handle_card(cid)

    key = keyboard.keypad_check()
    if len(key) > 0:
        handle_keyboard(key)

    display_date(':')
    time.sleep(0.1)

    led.blue_on()
