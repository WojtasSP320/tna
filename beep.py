#!/usr/bin/env python
# -*- coding: utf8 -*-

import time
import RPi.GPIO as GPIO
from led import led


class beep:

    BEEP_OUT = 18

    led = led()

    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.BEEP_OUT, GPIO.OUT, initial=GPIO.LOW)

    def beep(self, t):
        GPIO.output(self.BEEP_OUT, GPIO.HIGH)
        time.sleep(t)
        GPIO.output(self.BEEP_OUT, GPIO.LOW)

    def beep_ok(self):
        self.led.green_on()
        self.beep(0.05)
        self.led.green_off()
        time.sleep(0.05)
        self.led.green_on()
        self.beep(0.05)
        self.led.green_off()
        time.sleep(0.05)

    def beep_err(self):
        self.led.blue_on()
        self.beep(0.3)
        self.led.blue_off()
        time.sleep(0.3)
        self.led.blue_on()
        self.beep(0.3)
        self.led.blue_off()
        time.sleep(0.3)
        self.led.blue_on()
        self.beep(0.6)
        self.led.blue_off()
