#!/usr/bin/env python
# -*- coding: utf8 -*-

import socket
import urllib2
import json
from parameters import parameters


class rest:
    DANLINE_HOST = 'https://danline.system'
    HASH = 'ThisTokenIsNotSoSecretChangeIt'
    # HASH = '89dsd89b9s8b6f9sd86f96893w47f69a8sd0f6wb098e'

    EVENT_WORK = 0
    EVENT_BREAK = 1



    routing = {
        'last_log': '/card/reader/last/log/by/token/',  # {token}/{hash}
        'rfid_user_name': '/card/reader/rfid/user/name/',  # {token}/{hash}
        'log_working_time': '/card/reader/log/working/time/',  # {token}/{event}/{place}/{hash}
        'tna_reader_broadcast': '/card/reader/broadcast/',  # {place}/{hash}
    }

    parameters = None

    def __init__(self):
        self.parameters = parameters()

    def get(self, route, parameters):
        url = self.DANLINE_HOST + self.routing[route] + parameters
        print "Rest get: " + url
        try:
            response = urllib2.urlopen(url, timeout=15)
            data = response.read()
            data = json.loads(data)
            print data['message']
        except (socket.timeout, socket.error):
            print "Server timeout"
            data = {'success': False, 'message': 'bps', 'userNotFound': False}
        except urllib2.URLError:
            data = {'success': False, 'message': '500', 'userNotFound': False}
        except Exception as e:
            print "Unexpected error!"
            data = {'success': False, 'message': '', 'userNotFound': False}
        return data

    def get_last_log(self, token):
        parameters = '%s/%s' % (token, self.HASH)
        return self.get('last_log', parameters)

    def get_rfid_user_name(self, token):
        parameters = '%s/%s' % (token, self.HASH)
        return self.get('last_log', parameters)

    def log_working_time(self, token, event):
        parameters = '%s/%s/%s/%s' % (token, event, self.parameters.place, self.HASH)
        return self.get('log_working_time', parameters)

    def tna_reader_broadcast(self):
        parameters = '%s/%s' % (self.parameters.place, self.HASH)
        return self.get('log_working_time', parameters)

